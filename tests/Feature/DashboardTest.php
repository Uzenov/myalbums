<?php

namespace Tests\Feature;

use App\Models\Album;
use App\Models\Genre;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutVite();

        $this->user = User::factory()->create();
    }


    public function test_user_can_view_albums_songs_and_genres_on_the_dashboard()
    {
        Schema::disableForeignKeyConstraints();

        Storage::fake('public');

        $album = Album::factory()->create();

        $genre = Genre::factory()->create();

        $this->artisan('db:seed --class=GenreSeeder');

        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertStatus(200);
        $response->assertSee($album->title);
        $response->assertSee($genre->name);

        Schema::enableForeignKeyConstraints();

    }
}
