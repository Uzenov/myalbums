<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My albums</title>
    <link rel="stylesheet" href="{{ asset('pdf.css') }}" type="text/css">
</head>
<body>

<div class="margin-top">
    <table class="w-full">
        <tr>
            <td class="w-half">
                <div><h4>Title</h4></div>
                <div>{{$title}}</div>
            </td>
            <td class="w-half">
                <div><h4>Description</h4></div>
                <div>{{$description}}</div>
            </td>
            <td class="w-half">
                <div><h4>Count</h4></div>
                <div>{{$count}}</div>
            </td>
            <td class="w-half">
                <div><h4>Genres</h4></div>
                <div>{{$genres}}</div>
            </td>
        </tr>
    </table>
</div>

</body>
</html>