<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAlbumRequest;
use App\Http\Requests\UpdateAlbumRequest;
use App\Http\Resources\AlbumResource;
use App\Models\Album;
use App\Models\Genre;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class AlbumController extends Controller
{

    /**
     * Show the form for creating a new album.
     */
    public function create(): Response
    {
        $genres = Genre::select('id', 'name')->paginate(Genre::PAGINATION_LENGTH);

        return Inertia::render('CreateAlbum')->with([
            'genres' => $genres
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAlbumRequest $request): RedirectResponse
    {
        $validatedRequest = $request->validated();

        Album::create([
            'title' => $validatedRequest['title'],
            'description' => $validatedRequest['description'],
            'genres' => $validatedRequest['genres'],
            'count' => $validatedRequest['count']
        ]);

        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     */
    public function show(Album $album): Response
    {
        $albums = AlbumResource::collection(
            Album::select('id', 'title', 'count')
                ->orderBy('id', 'desc')
                ->paginate(Album::PAGINATION_LENGTH)
        );

        $genres = Genre::select('id', 'name')->paginate(Genre::PAGINATION_LENGTH);

        return Inertia::render('Dashboard')
            ->with([
                'albums' => $albums,
                'genres' => $genres,
                'filtered_album' => $album->title
            ]);
    }

    /**
     * Show the form for editing the specified album.
     */
    public function edit(Album $album): Response
    {
        $genres = Genre::select('id', 'name')->paginate(Genre::PAGINATION_LENGTH);
        return Inertia::render('EditAlbum')->with(['album' => $album, 'genres'=>$genres]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAlbumRequest $request, Album $album): RedirectResponse
    {
        $validatedRequest = $request->validated();

            $album->update([
                'title' => $validatedRequest['title'],
                'description' => $validatedRequest['description'],
                'genre' => $validatedRequest['genre'],
                'count' => $validatedRequest['count']
            ]);

        return redirect()->route('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Album $album): RedirectResponse
    {
        $album->delete();

        return redirect()->route('dashboard');
    }

    /**
     * Export albums data to PDF.
     */
    public function generatePDF(StoreAlbumRequest $request, Album $album)
    {
        $pdf = Pdf::loadView('pdf', [
            'title' => $request->title,
            'description' => $request->description,
            'count' => $request->count,
            'genres' => $request->genres
        ]);
        return $pdf->download('albums.pdf');
    }
}
