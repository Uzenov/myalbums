<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGenreRequest;
use App\Http\Requests\UpdateGenreRequest;
use App\Models\Genre;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class GenreController extends Controller
{
    /**
     * Display the specified genre.
     */
    public function show(Genre $genre): Response
    {

        $genres = Genre::select('id', 'name')->paginate(Genre::PAGINATION_LENGTH);

        return Inertia::render('Dashboard')
            ->with([
                'genres' => $genres,
                'filtered_genre' => $genre->name
            ]);
    }

    /**
     * Show the form for creating a new genre.
     */
    public function create(): Response
    {
        return Inertia::render('CreateGenre');
    }

    /**
     * Store a newly created genre in storage.
     */
    public function store(StoreGenreRequest $request): RedirectResponse
    {
        $validatedRequest = $request->validated();

        Genre::create([
            'name' => $validatedRequest['name'],
            'description' => $validatedRequest['description']
        ]);

        return redirect()->route('dashboard');
    }

    /**
     * Show the form for editing the genre.
     */
    public function edit(Genre $genre): Response
    {
        return Inertia::render('EditGenre')->with([
            'genre' => $genre
        ]);
    }

    /**
     * Update the specified genre in storage.
     */
    public function update(UpdateGenreRequest $request, Genre $genre): RedirectResponse
    {
        $validatedRequest = $request->validated();

        $genre->update([
            'name' => $validatedRequest['name'],
            'description' => $validatedRequest['description']
        ]);

        return redirect()->route('dashboard');
    }

    /**
     * Remove the specified genre from storage.
     */
    public function destroy(Genre $genre): RedirectResponse
    {
        $genre->delete();

        return redirect()->route('dashboard');
    }
}
