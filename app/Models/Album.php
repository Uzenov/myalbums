<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Album create(array $array)
 * @method static Album findOrFail(mixed $album_id)
 * @method static orderBy(string $string, string $string1)
 * @method static select(string $string, string $string1, string $string2)
 * @property string $title
 * @property string $description
 * @property string $count
 * @property string $genres
 */
class Album extends Model
{
    use HasFactory;

    const PAGINATION_LENGTH = 20;

    protected $guarded = [];

}
