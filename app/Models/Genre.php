<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Genre create(array $array)
 * @method static Genre findOrFail(mixed $genre_id)
 * @method static orderBy(string $string, string $string1)
 * @method static select(string $string, string $string1, string $string2)
 * @property string $name
 * @property string $description
 */
class Genre extends Model
{
    use HasFactory;

    const PAGINATION_LENGTH = 10;

    protected $guarded = [];
}
